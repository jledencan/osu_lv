# Zadatak 3.4.1 Skripta zadatak_1.py uˇcitava podatkovni skup iz data_C02_emission.csv.
# Dodajte programski kod u skriptu pomo´cu kojeg možete odgovoriti na sljede´ca pitanja:

import pandas as pd
data = pd. read_csv ( 'data_C02_emission.csv')

# a) Koliko mjerenja sadrži DataFrame? Kojeg je tipa svaka velicina? Postoje li izostale ili
# duplicirane vrijednosti? Obrišite ih ako postoje. Kategoricke velicine konvertirajte u tip
# category.

data.info()
print(f"There is:{data.duplicated().sum()} duplicated values")
data.drop_duplicates()

for column in ['Make', 'Model', 'Vehicle Class', 'Transmission', 'Fuel Type']:
    data[column] = data[column].astype('category')

data.info()

# b) Koja tri automobila ima najvecu odnosno najmanju gradsku potrošnju? Ispišite u terminal:
# ime proizvodaca, model vozila i kolika je gradska potrošnja.


sorted_data = data.sort_values(by='Fuel Consumption City (L/100km)')
sorted_data = sorted_data.reset_index(drop=True)
print(sorted_data.head(3)[['Make', 'Model', 'Fuel Consumption City (L/100km)']])
print(sorted_data.tail(3)[['Make', 'Model', 'Fuel Consumption City (L/100km)']]) 


# c) Koliko vozila ima velicinu motora izmedu 2.5 i 3.5 L? Kolika je prosjecna C02 emisija
# plinova za ova vozila?

engine_data=data[(data['Engine Size (L)']>= 2.5)& (data['Engine Size (L)']<=3.5)]
print(len(engine_data))

print(engine_data['CO2 Emissions (g/km)'].mean())



# d) Koliko mjerenja se odnosi na vozila proizvodaca Audi? Kolika je prosjecna emisija C02
# plinova automobila proizvodaca Audi koji imaju 4 cilindara?

audi = data[(data['Make'] == 'Audi')]
print(f"{len(audi)} Audi cars")

four_cylinders_audi = audi[(audi['Cylinders']) == 4]
print(four_cylinders_audi['CO2 Emissions (g/km)'].mean())


# e) Koliko je vozila s 4,6,8. . . cilindara? Kolika je prosjecna emisija C02 plinova s obzirom na
# broj cilindara?

grouped = data.groupby('Cylinders')

for group,cylinder_group in grouped:
    print(f"Number of vehicles with {group} cylinders: {len(cylinder_group)}")
    print(f"Mean for {group} cylinders: {cylinder_group['CO2 Emissions (g/km)'].mean()}")


# f)Kolika je prosjecna gradska potrošnja u sluˇcaju vozila koja koriste dizel, a kolika za vozila
#koja koriste regularni benzin? Koliko iznose medijalne vrijednosti?

grouped_fuel_types = data.groupby('Fuel Type')
for group,group_data in grouped_fuel_types:
    print(group)

diesel_vehicles_mean = data[data['Fuel Type'] == 'D']['Fuel Consumption City (L/100km)'].mean()
print(diesel_vehicles_mean)
diesel_consumption_median = (data[data['Fuel Type'] == 'D'])['Fuel Consumption City (L/100km)'].median()
print(diesel_consumption_median)

petrol_consumption_mean = (data[data['Fuel Type'] == 'X'])['Fuel Consumption City (L/100km)'].mean()
petrol_consumption_median = (data[data['Fuel Type'] == 'X'])['Fuel Consumption City (L/100km)'].median()
print(petrol_consumption_mean)
print(petrol_consumption_median) 

# g) Koje vozilo s 4 cilindra koje koristi dizelski motor ima najvecu gradsku potrošnju goriva?

vehicles_wih_diesel_motor_4_cylinders = data[(data['Fuel Type'] == 'D')  &  (data['Cylinders'] == 4)]
max_city_consumption_D4 = vehicles_wih_diesel_motor_4_cylinders['Fuel Consumption City (L/100km)'].max()
result = vehicles_wih_diesel_motor_4_cylinders[vehicles_wih_diesel_motor_4_cylinders['Fuel Consumption City (L/100km)'] == max_city_consumption_D4]
print(result)

# h) Koliko ima vozila ima ruˇcni tip mjenjaˇca (bez obzira na broj brzina)?

print(len(data[data['Transmission'].str.contains('M') & ~(data['Transmission'].str.contains('AM'))]))


# i) Izracunajte korelaciju izmed¯u numericˇkih velicˇina. Komentirajte dobiveni rezultat.

print(data.corr())
