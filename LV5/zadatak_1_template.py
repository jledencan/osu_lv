import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split


X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)


#a) Prikažite podatke za ucenje u x1−x2 ravnini matplotlib biblioteke pri cemu podatke obojite
# s obzirom na klasu. Prikažite i podatke iz skupa za testiranje, ali za njih koristite drugi
# marker (npr. ’x’). Koristite funkciju scatter koja osim podataka prima i parametre c i
# cmap kojima je moguce definirati boju svake klase.

from matplotlib.colors import ListedColormap

plt.scatter(X_train[:,0], X_train[:,1], c=y_train, cmap=ListedColormap({'pink', 'blue'}))
plt.scatter(X_test[:,0], X_test[:,1], c=y_test, cmap=ListedColormap({'orange','black'}), marker='x')
plt.show()



# b) Izgradite model logisticke regresije pomocu scikit-learn biblioteke na temelju skupa podataka
# za ucenje.

from sklearn . linear_model import LogisticRegression

# inicijalizacija i ucenje modela logisticke regresije
model = LogisticRegression ()
model.fit( X_train , y_train )
# predikcija na skupu podataka za testiranje
y_test_p = model.predict ( X_test )


# c) Pronadite u atributima izgradenog modela parametre modela. Prikažite granicu odluke
# naucenog modela u ravnini x1 −x2 zajedno s podacima za ucenje. Napomena: granica
# odluke u ravnini x1−x2 definirana je kao krivulja: θ0+θ1x1+θ2x2 = 0.

t0=model.intercept_[0] #theta0
t1, t2=model.coef_.T #theta1, theta2

#x1= -theta0/theta1 -(theta2/theta1)*x2
#x1=c+m

c = -t0/t1
m = -t2/t1

xd = np.array([X_train[:,1].min(), X_train[:,1].max()])
yd=c+m*xd

plt.plot(xd,yd,'k')
plt.show()
plt.fill_between(xd,yd, -4,color='tab:orange')
plt.fill_between(xd,yd, 4,color='tab:blue')


# d) Provedite klasifikaciju skupa podataka za testiranje pomocu izgradenog modela logisticke
# regresije. Izracunajte i prikažite matricu zabune na testnim podacima. Izracunate tocnost,
# preciznost i odziv na skupu podataka za testiranje.

import numpy as np
import matplotlib . pyplot as plt
from sklearn.metrics import accuracy_score, classification_report 
from sklearn.metrics import confusion_matrix , ConfusionMatrixDisplay

# stvarna vrijednost izlazne velicine i predikcija
y_true = y_test
y_pred = y_test_p

# tocnost
print(" Tocnost : " , accuracy_score (y_true , y_pred ))

# matrica zabune
cm = confusion_matrix ( y_true , y_pred )
print(" Matrica zabune : " , cm)
disp = ConfusionMatrixDisplay( confusion_matrix (y_true , y_pred ))
disp.plot()
plt.show()

# report
print( classification_report (y_true , y_pred ))


# e) Prikažite skup za testiranje u ravnini x1−x2. Zelenom bojom oznacite dobro klasificirane
# primjere dok pogrešno klasificirane primjere oznacite crnom bojom.