my_list=[]
my_input="initial"
counter=0

def average_value(lst):
    return sum(lst)/len(lst)


while(my_input != "Done"):
    my_input=input("Enter num: ")
    if((my_input >= 'A' and my_input <= 'Z') or (my_input >= 'a' and my_input <= 'z')):
        if(my_input != "Done"):
            print("Invalid input")
    elif my_input != "Done":
        my_list.append(float(my_input))
        counter += 1

print(f"You have entered {counter} numbers")

average = average_value(my_list)
min_value = min(my_list)
max_value = max(my_list)
my_list.sort()

print(f"Average value: {round(average, 2)}")
print(f"Min value: {min_value}")
print(f"Max value: {max_value}")
print(my_list)
