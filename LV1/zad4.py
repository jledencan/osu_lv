
file = open ('song.txt')
my_dictionary={}
counter = 0


for line in file :
    for word in line.split():
        if word in my_dictionary.keys():
            value=my_dictionary.get(word)
            my_dictionary.update({f"{word}": value+1})
        else:
            my_dictionary.update({f"{word}": 1})

for value in my_dictionary.values():
    if value == 1:
        counter +=1
            
print(f"Broj rijeci koje se 1 pojavljuju: {counter}")

for item in my_dictionary.items():
    print(item)

file.close ()