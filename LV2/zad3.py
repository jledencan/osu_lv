# Zadatak 2.4.3 Skripta zadatak_3.py ucitava sliku ’road.jpg’. Manipulacijom odgovaraju´ce
# numpy matrice pokušajte:
# a) posvijetliti sliku,
# b) prikazati samo drugu ˇcetvrtinu slike po širini,
# c) zarotirati sliku za 90 stupnjeva u smjeru kazaljke na satu,
# d) zrcaliti sliku.

import numpy as np
import matplotlib . pyplot as plt



img = plt.imread("road.jpg")
img = img [:,:,0].copy()
print(img.shape)
print(img.dtype)
plt.figure()

#a)
plt.imshow(img , cmap ="gray", alpha=0.2)
plt.show()

#b)
height, width= img.shape

start_col = width // 4
end_col = width //2
sliced_img = img[:, start_col:end_col]

plt.imshow(sliced_img,cmap ="gray" )

#Drugi nacin
# sliced_img= img[:,110:320]
# plt.imshow(sliced_img)
plt.show()


#c)
rotated_img = np.rot90(img, 3)
plt.imshow(rotated_img, cmap ="gray")
plt.title("Slika zarotirana za 90")
plt.show()


#d)
mirrored_img = np.fliplr(img)

fig, (ax1, ax2) = plt.subplots(1, 2)
ax1.imshow(img, cmap ="gray")
ax1.set_title("Original Image")
ax2.imshow(mirrored_img, cmap ="gray")
ax2.set_title("Mirrored Image")

plt.show()