#2.4.2 Datoteka data.csv sadrži mjerenja visine i mase provedena na muškarcima i
# ženama. Skripta zadatak_2.py uˇcitava dane podatke u obliku numpy polja data pri ˇcemu je u
# prvom stupcu polja oznaka spola (1 muško, 0 žensko), drugi stupac polja je visina u cm, a treci
# stupac polja je masa u kg.

# a) Na temelju velicine numpy polja data, na koliko osoba su izvršena mjerenja?
# b) Prikažite odnos visine i mase osobe pomocu naredbe matplotlib.pyplot.scatter.
# c) Ponovite prethodni zadatak, ali prikažite mjerenja za svaku pedesetu osobu na slici.
# d) Izraˇcunajte i ispišite u terminal minimalnu, maksimalnu i srednju vrijednost visine u ovom
# podatkovnom skupu.
# e) Ponovite zadatak pod d), ali samo za muškarce, odnosno žene. Npr. kako biste izdvojili
# muškarce, stvorite polje koje sadrži bool vrijednosti i njega koristite kao indeks retka.
# ind = (data[:,0] == 1)

import numpy as np
import matplotlib.pyplot as plt
from statistics import mean

import csv

with open("data.csv", 'r') as x:
    sample_data = list(csv.reader(x, delimiter=","))

sample_data = np.array(sample_data)
print(sample_data)


#a) 
#print (sample_data.shape) # koliko redaka ima vektor
print(len(sample_data)-1)


# b)
s,height,weight = sample_data.T.tolist()
s=np.delete(s,0)

height=np.delete(height,0) 
weight=np.delete(weight,0) 

h = height.astype(float)
w = weight.astype(float)

plt.scatter(h, w, color = '#9EE7F5', s=1)
plt.show()

# c)
fheight = [h[i] for i in range(len(height)) if i%50==0]
fweight = [w[i] for i in range(len(weight)) if i%50==0]


plt.scatter(fheight, fweight, color = '#9EE7F5', s=1)

plt.xlabel ('Height')
plt.ylabel ('Weight')
plt.title ( 'Scatter plot')

plt.show()


# d)

h = height.astype(float)
print(f"Min height: {min(h)}")
print(f"Max height: {max(h)}")
print(f"Arithmetic value: {mean(h)}")


# e) 

s = s.astype(float)
ind = (s == 1.0) #male
mh = [h[i] for i in range(len(h)) if ind[i]]
fh = [h[i] for i in range(len(h)) if ind[i] == False]

# m =(sample_data[:, 0] ==1.0)
# mh = [h[i] for i in range(len(h)) if m[i]]
# mh =(sample_data[m, 1])
# f =(sample_data[:, 0] == 0.0)
# fh =(sample_data[f, 1])

print(f"Min men height: {min(mh)}")
print(f"Max men height: {max(mh)}")
print(f"Arithmetic value men: {mean(mh)}")

print(f"Min women height: {min(fh)}")
print(f"Max women height: {max(fh)}")
print(f"Arithmetic value women: {mean(fh)}")