# Zadatak 4.5.1 Skripta zadatak_1.py ucitava podatkovni skup iz data_C02_emission.csv.
# Potrebno je izgraditi i vrednovati model koji procjenjuje emisiju C02 plinova na temelju ostalih
# numerickih ulaznih velicina. Detalje oko ovog podatkovnog skupa mogu se pronaci u 3.
# laboratorijskoj vježbi.

import matplotlib.pyplot as plt
from statistics import mean
import pandas as pd

# a) Odaberite željene numericke velicine specificiranjem liste s nazivima stupaca. Podijelite
# podatke na skup za ucenje i skup za testiranje u omjeru 80%-20%.

data = pd.read_csv('data_C02_emission.csv')
input_variables=['Fuel Consumption City (L/100km)',
                 'Fuel Consumption Hwy (L/100km)',
                 'Fuel Consumption Comb (L/100km)',
                 'Fuel Consumption Comb (mpg)',
                 'Engine Size (L)',
                 'Cylinders']

output_variable = ['CO2 Emissions (g/km)']
X = data[input_variables].to_numpy()
y = data[output_variable].to_numpy()


from sklearn.model_selection import train_test_split

# podijeli skup na podatkovni skup za ucenje i podatkovni skup za testiranje
X_train , X_test , y_train , y_test = train_test_split (X, y, test_size = 0.2, random_state =1)
print(X_train)
print(y_train)

# b) Pomocu matplotlib biblioteke i dijagrama raspršenja prikažite ovisnost emisije C02 plinova
# o jednoj numerickoj velicini. Pri tome podatke koji pripadaju skupu za ucenje oznacite
# plavom bojom, a podatke koji pripadaju skupu za testiranje oznacite crvenom bojom.


plt.scatter (x = X_train[:,4], y= y_train[:, 0], color= 'b', s=8, label='train')
plt.scatter (x = X_test[:,4], y= y_test[:, 0], color= 'r', s=8, label='test')
plt.xlabel('Engine Size (L)')
plt.ylabel('CO2 Emissions (g/km)')

plt.legend()

plt.show ()

# c) Izvršite standardizaciju ulaznih velicina skupa za ucenje. Prikažite histogram vrijednosti
# jedne ulazne velicine prije i nakon skaliranja. Na temelju dobivenih parametara skaliranja
# transformirajte ulazne velicine skupa podataka za testiranje.

from sklearn.preprocessing import MinMaxScaler
# min - max skaliranje
sc = MinMaxScaler ()
X_train_n = sc.fit_transform( X_train )
X_test_n = sc.transform( X_test )


plt.hist( X_train[:, 4], bins = 10)
plt.figure()
plt.hist(X_train_n[:, 4], bins = 10)
plt.show()

# d) Izgradite linearni regresijski modeli. Ispišite u terminal dobivene parametre modela i
# povežite ih s izrazom 4.6.

import sklearn.linear_model as lm

linearModel = lm.LinearRegression()
linearModel.fit( X_train_n , y_train )
print(f'Parametri modela: {linearModel.coef_}')

# e) Izvršite procjenu izlazne velicine na temelju ulaznih velicina skupa za testiranje. Prikažite
# pomocu dijagrama raspršenja odnos izmedu stvarnih vrijednosti izlazne velicine i procjene
# dobivene modelom.

from sklearn.metrics import mean_absolute_error
# predikcija izlazne velicine na skupu podataka za testiranje
y_test_p = linearModel.predict( X_test_n )#vraca numpy array

plt.scatter(X_test_n[:, 0],y_test, c='b', label='Real values', s=1.5) #Fuel Consumption City (L/100km)
plt.scatter(X_test_n[:, 0],y_test_p, c='g', label='Predicted values', s=1.5)

plt.xlabel('Fuel Consumption City (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.legend()
plt.show()



# f) Izvršite vrednovanje modela na nacin da izracunate vrijednosti regresijskih metrika na
# skupu podataka za testiranje.

# evaluacija modela na skupu podataka za testiranje pomocu MAE
MAE = mean_absolute_error ( y_test , y_test_p )

from sklearn.metrics import mean_squared_error, mean_absolute_percentage_error, r2_score

print(f'Mean squared error: {mean_squared_error(y_test, y_test_p)}')
print(f'Mean absolute error: {mean_absolute_error(y_test, y_test_p)}')
print(f'Mean absolute percentage error: {mean_absolute_percentage_error(y_test, y_test_p)}%')
print(f'R2 score: {r2_score(y_test, y_test_p)}')

# g) Što se dogada s vrijednostima evaluacijskih metrika na testnom skupu kada mijenjate broj
# ulaznih velicina?

import numpy as np

#smanjenjem za 1 ulaznu velicinu
new_test_values = np.delete(X_test_n, 5, axis=1)
new_train_values = np.delete(X_train_n, 5 , axis=1)

sc = MinMaxScaler()

X_train_scaled=sc.fit_transform(new_train_values)

X_test_new = sc.transform( new_test_values )
X_train_new = sc.transform( new_train_values )

linearModel = lm.LinearRegression()
linearModel.fit( X_train_scaled , y_train )


y_new_test_p = linearModel.predict(X_test_new)

MSE=mean_squared_error(y_test, y_new_test_p)
MAE=mean_absolute_error(y_test, y_new_test_p)
MAPE=mean_absolute_percentage_error(y_test, y_new_test_p)
R2=r2_score(y_test, y_new_test_p)

print(f'New MSE: {MSE}')
print(f'New MAE: {MAE}')
print(f'New MAPE: {MAPE}%')
print(f'New R2 score: {R2}')



#smanjenjem za 2 ulazne velicine
new_test_values2 = np.delete(new_test_values, 4, axis=1)
new_train_values2 = np.delete(new_train_values, 4 , axis=1)

sc = MinMaxScaler()

X_train_scaled2=sc.fit_transform(new_train_values2)

X_test_new2 = sc.transform( new_test_values2 )
X_train_new2 = sc.transform( new_train_values2 )

linearModel = lm.LinearRegression()
linearModel.fit( X_train_scaled2 , y_train )


y_new_test_p2 = linearModel.predict(X_test_new2)

MSE_2=mean_squared_error(y_test, y_new_test_p2)
MAE_2=mean_absolute_error(y_test, y_new_test_p2)
MAPE_2=mean_absolute_percentage_error(y_test, y_new_test_p2)
R2_2=r2_score(y_test, y_new_test_p2)

print(f'New 2 MSE: {MSE_2}')
print(f'New 2 MAE: {MAE_2}')
print(f'New 2 MAPE: {MAPE_2}%')
print(f'New 2 R2 score: {R2_2}')


#Povecavaju se sve vrijednosti smanjenjem ulaznih velicina osim R2 koji se smanjuje






