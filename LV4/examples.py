import numpy as np
import matplotlib.pyplot as plt
from statistics import mean

import pandas as pd
data = pd. read_csv ( 'data_C02_emission.csv')

#4.1
from sklearn import datasets
from sklearn.model_selection import train_test_split

# ucitaj ugradeni podatkovni skup
X, y = datasets.load_diabetes ( return_X_y = True )
# podijeli skup na podatkovni skup za ucenje i podatkovni skup za testiranje
X_train , X_test , y_train , y_test = train_test_split (X, y, test_size = 0.2, random_state =1)
print(X_train)
print(y_train)

#4.2
from sklearn.preprocessing import MinMaxScaler

# min - max skaliranje
sc = MinMaxScaler ()
X_train_n = sc.fit_transform ( X_train )
X_test_n = sc.transform ( X_test )
print(X_train_n)

#4.3
from sklearn.preprocessing import OneHotEncoder

ohe = OneHotEncoder ()
X_encoded = ohe.fit_transform ( data[['Fuel Type']]).toarray()
print(X_encoded)

#4.4
import sklearn . linear_model as lm

linearModel = lm. LinearRegression ()
linearModel.fit( X_train_n , y_train )


#4.5
from sklearn . metrics import mean_absolute_error

# predikcija izlazne velicine na skupu podataka za testiranje
y_test_p = linearModel.predict ( X_test_n )
# evaluacija modela na skupu podataka za testiranje pomocu MAE
MAE = mean_absolute_error ( y_test , y_test_p )
print(MAE)