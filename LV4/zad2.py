#Zadatak 4.5.2 Na temelju rješenja prethodnog zadatka izradite model koji koristi i kategoricku
#varijable „Fuel Type“ kao ulaznu velicinu. Pri tome koristite 1-od-K kodiranje kategorickih
#velicina. Radi jednostavnosti nemojte skalirati ulazne velicine. Komentirajte dobivene rezultate.
#Kolika je maksimalna pogreška u procjeni emisije C02 plinova u g/km? O kojem se modelu
#vozila radi?

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import max_error
import sklearn.linear_model as lm


data = pd.read_csv('data_C02_emission.csv')
input_variables=['Fuel Consumption City (L/100km)',
                 'Fuel Consumption Hwy (L/100km)',
                 'Fuel Consumption Comb (L/100km)',
                 'Fuel Consumption Comb (mpg)',
                 'Engine Size (L)',
                 'Cylinders', 
                 'Fuel Type'] #kategoricka velicina

output_variable = ['CO2 Emissions (g/km)']

#one_hot_encoded_data = pd.get_dummies(data, columns = ['Fuel Type'])
#print(one_hot_encoded_data)

# Converting type of columns to category
data['Fuel Type'] = data['Fuel Type'].astype('category')
  
# Assigning numerical values and storing it in another columns
data['Fuel Type'] = data['Fuel Type'].cat.codes

# Create an instance of One-hot-encoder
enc = OneHotEncoder()

X = data[['Engine Size (L)','Cylinders','Fuel Type','Fuel Consumption City (L/100km)','Fuel Consumption Hwy (L/100km)','Fuel Consumption Comb (L/100km)','Fuel Consumption Comb (mpg)']]
y = data[['CO2 Emissions (g/km)']] 
#X = data[input_variables]

# Passing encoded columns
enc_data = enc.fit_transform(X[['Fuel Type']]).toarray()


X = X.to_numpy()
y = data[output_variable].to_numpy()


X_train , X_test , y_train , y_test = train_test_split (X, y, test_size = 0.2, random_state =1)
print(X_train)
print(y_train)


linearModel = lm.LinearRegression()
linearModel.fit( X_train , y_train )


y_true = y_test
y_pred = linearModel.predict(X_test)

max_error_value = max_error(y_true, y_pred)
print(max_error_value)

mev=np.argmax(max_error_value)

print(data.iloc[mev]) #Iz izlistanih podataka vidi se da je model ILX
print(data.iloc[mev]['Model'])

